package shadow.sandbox.common.filter;

import reactor.core.publisher.Mono;
import shadow.sandbox.common.model.RequestData;
import shadow.sandbox.common.storage.ResponseByRequestStorage;

public class IntersectionRequestFilter implements RequestFilter {

    private ResponseByRequestStorage responseByRequestStorage;

    public IntersectionRequestFilter(ResponseByRequestStorage responseByRequestStorage) {
        this.responseByRequestStorage = responseByRequestStorage;
    }

    @Override
    public Mono<RequestData> filter(RequestData requestData) {
        return Mono.defer(() -> hasAnyRouteWithIntersection(requestData)
            .flatMap(hasIntersection -> {
                if (hasIntersection) {
                    return Mono.empty();
                } else {
                    return Mono.just(requestData);
                }
            }));
    }

    private Mono<Boolean> hasAnyRouteWithIntersection(RequestData data) {
        return responseByRequestStorage
            .getRequests()
            .map(requestsDataCollection -> {
                for (RequestData keyData : requestsDataCollection) {
                    if (keyData.getMethod().equals(data.getMethod())) {
                        String[] keyDataPathParts = keyData.getPath().split("/");
                        String[] dataPathParts = data.getPath().split("/");
                        if (keyDataPathParts.length == dataPathParts.length) {
                            int keyDataEntryCount = 0;
                            int dataEntryCount = 0;
                            for (int i = 0; i < keyDataPathParts.length; i++) {
                                String keyDataPathPart = keyDataPathParts[i];
                                String dataPathPart = dataPathParts[i];
                                if (keyDataPathPart.equals("{}") && dataPathPart.equals("{}")) {
                                    keyDataEntryCount += 1;
                                    dataEntryCount += 1;
                                } else
                                if (keyDataPathPart.equals("{}")) {
                                    keyDataEntryCount += 1;
                                } else if (dataPathPart.equals("{}")) {
                                    dataEntryCount += 1;
                                } else if (!keyDataPathPart.equals(dataPathPart)) {
                                    keyDataEntryCount = 0;
                                    dataEntryCount = 0;
                                    break;
                                }
                            }
                            if (keyDataEntryCount != 0 && dataEntryCount != 0) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            });
    }
}
