package shadow.sandbox.common.filter;

import reactor.core.publisher.Mono;
import shadow.sandbox.common.model.RequestData;

public interface RequestFilter {

    Mono<RequestData> filter(RequestData requestData);
}
