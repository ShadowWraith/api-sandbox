package shadow.sandbox.common.model;

public enum HttpMethod {

    POST,
    PATCH,
    GET,
    DELETE
}
