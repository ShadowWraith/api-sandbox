package shadow.sandbox.common.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ResponseData {

    private final int statusCode;
    private final String body;
}
