package shadow.sandbox.common.model.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ApiMessage {

    NOT_ADDED(1),
    INTERNAL_ERROR(10000),
    NO_RESPONSE(2);

    private final int code;
}
