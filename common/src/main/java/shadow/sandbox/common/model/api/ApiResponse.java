package shadow.sandbox.common.model.api;

import lombok.Getter;

@Getter
public class ApiResponse {

    private final int code;
    private final String message;

    public ApiResponse(ApiMessage apiMessage) {
        this.code = apiMessage.getCode();
        this.message = apiMessage.name();
    }
}
