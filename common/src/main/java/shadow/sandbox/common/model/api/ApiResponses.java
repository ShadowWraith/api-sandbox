package shadow.sandbox.common.model.api;

public final class ApiResponses {

    public static final ApiResponse NOT_ADDED = new ApiResponse(ApiMessage.NOT_ADDED);
    public static final ApiResponse INTERNAL_SERVER_ERROR = new ApiResponse(ApiMessage.INTERNAL_ERROR);
    public static final ApiResponse NO_RESPONSE = new ApiResponse(ApiMessage.NO_RESPONSE);
}