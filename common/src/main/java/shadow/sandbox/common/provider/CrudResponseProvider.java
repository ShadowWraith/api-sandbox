package shadow.sandbox.common.provider;


import reactor.core.publisher.Mono;
import shadow.sandbox.common.model.RequestData;
import shadow.sandbox.common.model.ResponseData;

public interface CrudResponseProvider extends MonoResponseProvider {

    Mono<Boolean> addRoute(RequestData data, ResponseData responseData);
}
