package shadow.sandbox.common.provider;

import reactor.core.publisher.Mono;
import shadow.sandbox.common.model.RequestData;
import shadow.sandbox.common.model.ResponseData;

@FunctionalInterface
public interface MonoResponseProvider {

    Mono<ResponseData> get(RequestData requestData);
}
