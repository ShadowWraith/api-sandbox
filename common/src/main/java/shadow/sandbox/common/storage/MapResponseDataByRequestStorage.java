package shadow.sandbox.common.storage;

import reactor.core.publisher.Mono;
import shadow.sandbox.common.model.RequestData;
import shadow.sandbox.common.model.ResponseData;

import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ConcurrentHashMap;

public class MapResponseDataByRequestStorage implements ResponseByRequestStorage {

    private final Map<RequestData, ResponseData> responseByPath = new ConcurrentHashMap<>();

    @Override
    public Mono<ResponseData> get(RequestData data) {
        ResponseData responseData = responseByPath.get(data);
        if (responseData == null) {
            return Mono.error(new NoSuchElementException("No response found for path " + data.getPath()));
        }
        return Mono.just(responseData);
    }

    @Override
    public Mono<Collection<RequestData>> getRequests() {
        return Mono.just(responseByPath.keySet());
    }

    @Override
    public Mono<Boolean> add(RequestData requestData, ResponseData responseData) {
        return Mono.defer(() -> {
            responseByPath.put(requestData, responseData);
            return Mono.just(true);
        });
    }
}
