package shadow.sandbox.common.storage;

import reactor.core.publisher.Mono;
import shadow.sandbox.common.model.RequestData;
import shadow.sandbox.common.model.ResponseData;

import java.util.Collection;

public interface ResponseByRequestStorage {

    Mono<ResponseData> get(RequestData data);

    Mono<Collection<RequestData>> getRequests();

    Mono<Boolean> add(RequestData requestData, ResponseData responseData);
}
