package shadow.sandbox.webflux;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import shadow.sandbox.common.filter.IntersectionRequestFilter;
import shadow.sandbox.common.storage.MapResponseDataByRequestStorage;
import shadow.sandbox.common.storage.ResponseByRequestStorage;

@Configuration
public class AppBeanConfiguration {

    @Bean
    public ResponseByRequestStorage responseByRequestStorage() {
        return new MapResponseDataByRequestStorage();
    }

    @Bean
    public IntersectionRequestFilter intersectionRequestFilter(ResponseByRequestStorage responseByRequestStorage) {
        return new IntersectionRequestFilter(responseByRequestStorage);
    }
}
