package shadow.sandbox.webflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import shadow.sandbox.webflux.config.MultiEndpointConfig;

@SpringBootApplication
@EnableConfigurationProperties(MultiEndpointConfig.class)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
