package shadow.sandbox.webflux;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;
import shadow.sandbox.common.model.RequestData;
import shadow.sandbox.common.model.ResponseData;
import shadow.sandbox.common.model.api.ApiResponses;
import shadow.sandbox.webflux.config.MultiEndpointConfig;
import shadow.sandbox.webflux.model.EndpointMockRequest;
import shadow.sandbox.common.provider.CrudResponseProvider;
import shadow.sandbox.common.provider.MonoResponseProvider;

import java.util.NoSuchElementException;

@Configuration
public class RouteConfiguration {

    @Bean
    public RouterFunction<ServerResponse> configRoutes(
        MultiEndpointConfig multiEndpointConfig,
        Converter<ResponseData, Mono<ServerResponse>> responseDataToServerResponseConverter,
        Converter<ServerRequest, RequestData> serverRequestToRequestDataConverter,
        MonoResponseProvider responseProvider
    ) {
        RouterFunctions.Builder builder = RouterFunctions.route();
        multiEndpointConfig.getEndpoints().forEach(endpointConfig ->
            builder.route(
                RequestPredicates
                    .method(endpointConfig.getHttpMethod())
                    .and(RequestPredicates.path(endpointConfig.getPath())),
                serverRequest -> responseProvider
                    .get(serverRequestToRequestDataConverter.convert(serverRequest))
                    .flatMap(responseDataToServerResponseConverter::convert)
                    .onErrorResume(throwable -> {
                        if (throwable instanceof NoSuchElementException) {
                            return ServerResponse.status(403).bodyValue(ApiResponses.NO_RESPONSE);
                        } else {
                            return ServerResponse.status(500).bodyValue(ApiResponses.INTERNAL_SERVER_ERROR);
                        }
                    })
            )
        );
        return builder.build();
    }

    @Bean
    public RouterFunction<ServerResponse> addRoute(CrudResponseProvider crudResponseProvider) {
        return RouterFunctions.route(
            RequestPredicates.POST("/api/mock"),
            request -> request
                .bodyToMono(EndpointMockRequest.class)
                .flatMap(endpointMockRequest -> {
                    RequestData requestData = new RequestData(
                        endpointMockRequest.getRequestPath(),
                        endpointMockRequest.getRequestHttpMethod()
                    );
                    ResponseData responseData = new ResponseData(endpointMockRequest.getResponseHttpCode(), endpointMockRequest.getResponseBody());
                    return crudResponseProvider
                        .addRoute(requestData, responseData)
                        .flatMap(isAdded -> {
                            if (isAdded) {
                                return ServerResponse.ok().bodyValue("");
                            } else {
                                return ServerResponse.badRequest().bodyValue(ApiResponses.NOT_ADDED);
                            }
                        });
                })
                .onErrorResume(throwable -> ServerResponse.status(500).bodyValue(ApiResponses.INTERNAL_SERVER_ERROR))
        );
    }
}
