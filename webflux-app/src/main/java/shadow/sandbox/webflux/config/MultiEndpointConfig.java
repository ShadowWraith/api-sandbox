package shadow.sandbox.webflux.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.http.HttpMethod;

import java.util.List;

@ConfigurationProperties("application")
@ConstructorBinding
@AllArgsConstructor
@Getter
public class MultiEndpointConfig {

    private final List<EndpointConfig> endpoints;

    @AllArgsConstructor
    @Getter
    public static class EndpointConfig {

        private HttpMethod httpMethod;
        private String path;
    }
}
