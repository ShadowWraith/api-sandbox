package shadow.sandbox.webflux.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import shadow.sandbox.common.model.ResponseData;

@Component
public class ResponseDataToMonoServerResponseConverter implements Converter<ResponseData, Mono<ServerResponse>> {

    @Override
    public Mono<ServerResponse> convert(ResponseData source) {
        return ServerResponse
            .status(source.getStatusCode())
            .bodyValue(source.getBody());
    }
}
