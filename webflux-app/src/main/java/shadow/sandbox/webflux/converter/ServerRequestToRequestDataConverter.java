package shadow.sandbox.webflux.converter;


import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import shadow.sandbox.common.model.HttpMethod;
import shadow.sandbox.common.model.RequestData;

@Component
public class ServerRequestToRequestDataConverter implements Converter<ServerRequest, RequestData> {

    @Override
    public RequestData convert(ServerRequest source) {
        return new RequestData(source.path(), HttpMethod.valueOf(source.method().name()));
    }
}
