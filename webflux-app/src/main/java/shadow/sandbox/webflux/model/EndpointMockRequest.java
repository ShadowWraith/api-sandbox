package shadow.sandbox.webflux.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import shadow.sandbox.common.model.HttpMethod;

@Getter
public class EndpointMockRequest {

    private String requestPath;
    private HttpMethod requestHttpMethod;
    private int responseHttpCode;
    private String responseBody;

    @JsonCreator
    public EndpointMockRequest(
        @JsonProperty("requestPath") String requestPath,
        @JsonProperty("requestHttpMethod") HttpMethod requestHttpMethod,
        @JsonProperty("responseHttpCode") int responseHttpCode,
        @JsonProperty("responseBody") String responseBody
    ) {
        this.responseHttpCode = responseHttpCode;
        this.requestPath = requestPath;
        this.requestHttpMethod = requestHttpMethod;
        this.responseBody = responseBody;
    }
}
