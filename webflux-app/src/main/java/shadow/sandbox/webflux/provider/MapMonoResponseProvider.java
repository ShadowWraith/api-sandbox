package shadow.sandbox.webflux.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import shadow.sandbox.common.filter.RequestFilter;
import shadow.sandbox.common.model.RequestData;
import shadow.sandbox.common.model.ResponseData;
import shadow.sandbox.common.provider.CrudResponseProvider;
import shadow.sandbox.common.storage.ResponseByRequestStorage;

import java.util.List;

@Component
public class MapMonoResponseProvider implements CrudResponseProvider {

    private final ResponseByRequestStorage responseByRequestStorage;
    private final List<RequestFilter> requestFilters;

    @Autowired
    public MapMonoResponseProvider(ResponseByRequestStorage responseByRequestStorage, List<RequestFilter> requestFilters) {
        this.responseByRequestStorage = responseByRequestStorage;
        this.requestFilters = requestFilters;
    }

    @Override
    public Mono<ResponseData> get(RequestData requestData) {
        return responseByRequestStorage.get(requestData);
    }

    @Override
    public Mono<Boolean> addRoute(RequestData requestData, ResponseData responseData) {
        return Mono.defer(() -> {
            Mono<RequestData> requestDataMono = Mono.just(requestData);
            for (RequestFilter requestFilter : requestFilters) {
                requestDataMono = requestDataMono.flatMap(requestFilter::filter);
            }
            return requestDataMono
                .flatMap(requestData_ -> responseByRequestStorage.add(requestData_, responseData))
                .switchIfEmpty(Mono.just(false));
        });
    }
}
